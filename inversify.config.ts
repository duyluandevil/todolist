import { Container } from "inversify";
import { TasksController } from "./modules/tasks/tasks.controller"
import { TasksService } from "./modules/tasks/tasks.service"
import { TasksRepository } from "./modules/tasks/tasks.repository"
import { TasksItemsRepository } from "./modules/tasks_items/tasks.items.repository"

const container = new Container();

container.bind<TasksRepository>(TasksRepository).toSelf().inSingletonScope();
container.bind<TasksController>(TasksController).toSelf().inSingletonScope();
container.bind<TasksService>(TasksService).toSelf().inSingletonScope();

container.bind<TasksItemsRepository>(TasksItemsRepository).toSelf().inSingletonScope();

export default container;