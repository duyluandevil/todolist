import "reflect-metadata"
import { InversifyExpressServer } from 'inversify-express-utils'
import * as bodyParser from 'body-parser';

import container from "./inversify.config";
import { Config } from "./config/config.class"

const PORT = Config.getInstance().PORT

let server = new InversifyExpressServer(container, null, { rootPath: "/api/" });
server.setConfig((app: any) => {
    // add body parser
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(bodyParser.json());
});

let app = server.build();
app.listen(PORT, () => console.log(`server is running at port ${PORT}`));