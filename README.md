# ToDoList

## Description
Make an easy app Todo List with
- List all tasks, can custom list with parameters: limit, offset and search with name
- Update task content or status
- Delete task

## Library/ Framework
ExpressJS, Knex.js, InversifyJS

## UML
![](erd.png)

## Command in terminal
- Build project:  `npx tsc`
- Run project: `npm run start`