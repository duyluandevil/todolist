import * as dotenv from "dotenv"
dotenv.config()

export class Config {
    private static instance: Config;

    private constructor() { }

    static getInstance() {
        if (!Config.instance) {
            Config.instance = new Config();
        }
        return Config.instance;
    }

    PORT: any = process.env.PORT || 3000;
    DATABASE_CLIENT: string = process.env.DATABASE_CLIENT || "postgresql"
    DATABASE_NAME: string = process.env.DATABASE_NAME || "todolist";
    DATABASE_USER: string = process.env.POSTGRES_USER || "postgres";
    DATABASE_PASS: string = process.env.POSTGRES_PASSWORD || "changeme";
    DATABASE_HOST: string = process.env.DATABASE_HOST || "localhost";

}