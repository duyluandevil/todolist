import Knex from 'knex';
import { Config } from "./config.class"

export class Database {
    private static instance: Database;
    private knex: any;

    private constructor() {
        const config = Config.getInstance();

        this.knex = Knex({
            client: config.DATABASE_CLIENT,
            connection: {
                database: config.DATABASE_NAME,
                user: config.DATABASE_USER,
                password: config.DATABASE_PASS,
                host: config.DATABASE_HOST
            }
        });
    }

    public static getInstance(): Database {
        if (!Database.instance) {
            Database.instance = new Database();
        }

        return Database.instance;
    }

    public async run(query: string, params: any[] = []): Promise<any> {
        return this.knex.raw(query, params);
    }

    public query(){
        return this.knex
    }
}