import { injectable, inject } from "inversify";
import { TasksRepository } from "./tasks.repository";
import { TasksItemsRepository } from "../tasks_items/tasks.items.repository"

type TASK_RAW = {
    id: number,
    name: string,
    status: boolean,
    task_items_id: number,
    task_items_status: boolean,
    task_items_name: string,
    task_items_parent_id: number
}

type TASK_ITEMS = {
    id: number,
    name: string,
    status: boolean,
    child: TASK_CHILD[]
}

type TASK_CHILD = {
    id: number,
    name: string,
    status: boolean,
    task_id: number
}

@injectable()
export class TasksService {

    constructor(
        @inject(TasksRepository) private tasksRepository: TasksRepository,
        @inject(TasksItemsRepository) private tasksItemsRepository: TasksItemsRepository
    ) { }

    public async readItems(limit: any, offset: any, search: any) {
        (!limit || Number.isNaN(+limit)) && (limit = 10);
        (!offset || Number.isNaN(+offset)) && (offset = 0);

        if (search && Object.keys(search).length > 0) {
            Object.keys(search).forEach(e => {
                search[`task.${e}`] = search[e]

                delete search[e]
            })
        }

        let taskRaw: TASK_RAW[] = await this.tasksRepository.readAll(limit, offset, search)

        let taskItems: Record<string, TASK_ITEMS> = {}
        taskRaw.forEach((e: TASK_RAW) => {
            if (!taskItems[e?.id])
                taskItems[e?.id] = {
                    id: e?.id,
                    status: e?.status,
                    name: e?.name,
                    child: []
                }

            if (e?.task_items_id) {
                taskItems[e.id].child.push({
                    id: e.task_items_id,
                    status: e.task_items_status,
                    name: e.task_items_name,
                    task_id: e.task_items_parent_id
                })
            }

        })

        let data = [];
        for (const value of Object.values(taskItems)) {
            data.push(value);
        }

        return data
    }



    public updateItem(field: any, id: number): any {
        return this.tasksRepository.updateOneByID(field, id)
    }

    public countItems() {
        return this.tasksRepository.countAll()
    }

    public deleteItem(id: number) {
        return this.tasksRepository.deleteOneByID(id)
    }

}