import { inject } from "inversify";
import { controller, httpGet, httpPatch, httpDelete, request, response, next } from "inversify-express-utils";
import * as express from "express";

import { TasksService } from "./tasks.service";

@controller("/tasks")
export class TasksController {

    constructor(
        @inject(TasksService) private tasksService: TasksService,
    ) { }

    @httpGet("/")
    private async getAll(@request() req: express.Request, @response() res: express.Response, @next() next: express.NextFunction): Promise<object> {
        const {
            limit,
            offset,
            search
        } = req.query

        const items = await this.tasksService.readItems(limit, offset, search)
        return res.status(200).json({
            data: items
        })
    }

    @httpGet("/count")
    private async countAll(@request() req: express.Request, @response() res: express.Response, @next() next: express.NextFunction): Promise<object> {
        const items = await this.tasksService.countItems()
        return res.status(200).json({
            data: +items
        })
    }

    @httpPatch("/:id")
    private async updateItem(@request() req: express.Request, @response() res: express.Response, @next() next: express.NextFunction): Promise<object> {
        let body = req.body
        let {
            id
        } = req.params

        if (Number.isNaN(+id)) {
            return res.status(400).json({
                message: "id must number",
                data: {
                    isUpdate: false
                }
            })
        }

        const items = await this.tasksService.updateItem(body, +id)
        return res.status(200).json({
            message: "updated successful",
            data: {
                isUpdate: items
            }
        })
    }

    @httpDelete("/:id")
    private async deleteItem(@request() req: express.Request, @response() res: express.Response, @next() next: express.NextFunction): Promise<object> {
        let {
            id
        } = req.params

        if (Number.isNaN(+id)) {
            return res.status(400).json({
                message: "id must number",
                data: {
                    isDelete: false
                }
            })
        }

        const items = await this.tasksService.deleteItem(+id)
        return res.status(200).json({
            message: "deleted successful",
            data: {
                isUpdate: items
            }
        })
    }
}