import { injectable } from "inversify";
import { Database } from "../../config/knex.class"

@injectable()
export class TasksRepository {

    private database: Database
    constructor() {
        this.database = Database.getInstance()
    }

    public readAll(limit: number, offset: number, search: any) {
        return this.database.query()
            .select(
                "task.id as id",
                "task.status as status",
                "task.name as name",
                "task_items.id as task_items_id",
                "task_items.status as task_items_status",
                "task_items.name as task_items_name",
                "task_items.task_id as task_items_parent_id"
            )
            .from("task")
            .leftJoin("task_items", function () {
                this
                    .on("task.id", "=", "task_items.task_id")
            })
            .where({
                ...search
            })
            .whereIn(
                "task.id",
                function() {
                    this.select('id').from('task').limit(limit).offset(offset);
                })
            .catch((error: any) => {
                console.log({ error });
                return []
            })
    }

    public read(limit: number, offset: number, search: any) {
        return this.database.query()
            .select("*")
            .from("task")
            .where({
                ...search
            })
            .limit(limit)
            .offset(offset)
            .catch((error: any) => {
                console.log({ error });
                return []
            })
    }

    public countAll() {
        return this.database.query()
            .from("task")
            .count("id")
            .then((e: any) => {
                return {
                    count: +e[0]?.count
                }
            })
            .catch((error: any) => {
                console.log({ error });
                return []
            })
    }

    public updateOneByID(field: any, id: number) {
        return this.database.query()
            .from("task")
            .where('id', id)
            .update({
                ...field
            })
            .then(() => true)
            .catch((error: any) => {
                console.log({ error });
                return false
            })
    }

    public deleteOneByID(id: number) {
        return this.database.query()
            .from("task")
            .where('id', id)
            .del()
            .then(() => true)
            .catch((error: any) => {
                console.log({ error });
                return false
            })
    }

}