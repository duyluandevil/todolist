import { injectable } from "inversify";
import { Database } from "../../config/knex.class"

@injectable()
export class TasksItemsRepository {

    private database: Database
    constructor() {
        this.database = Database.getInstance()
    }

    public readAll(limit: number, offset: number, filter: any) {
        return this.database.query()
            .select("*")
            .where({
                ...filter
            })
            .from("task_items")
            .limit(limit)
            .offset(offset)
            .catch((error: any) => {
                console.log({ error });
                return []
            })
    }

}