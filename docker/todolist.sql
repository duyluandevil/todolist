-- This script was generated by the ERD tool in pgAdmin 4.
-- Please log an issue at https://redmine.postgresql.org/projects/pgadmin4/issues/new if you find any bugs, including reproduction steps.
BEGIN;


CREATE TABLE IF NOT EXISTS public.task
(
    id integer NOT NULL DEFAULT nextval('task_id_seq'::regclass),
    status boolean NOT NULL,
    name character varying(200) COLLATE pg_catalog."default",
    CONSTRAINT task_pkey PRIMARY KEY (id)
);
END;